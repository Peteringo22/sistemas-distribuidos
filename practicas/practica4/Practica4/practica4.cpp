#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define ELEMS 20000000 // Elementos a ordenar
int errors(int *numbers, int elems);
void initArr(int *numbers, int elems);
void SortArr(int *numbers);
int arr[ELEMS];
int main()
{
	int i;
	int n;
	clock_t start, stop;
	initArr(arr, ELEMS);
	start = clock();
	SortArr(arr);
	stop = clock();
	if ((n = errors(arr, ELEMS)))
		printf("Se encontrarin %d errores\n", n);
	else
		printf("%d elementos ordenados en %1.2f segundos\n", ELEMS, ((float)stop - (float)start) / 1000.0);
}
void initArr(int *numbers, int elems)
{
	int i;
	for (i = 0; i < elems; i++)
		numbers[i] = rand()*rand() % ELEMS;

	
}
int errors(int *numbers, int elems)
{
	int i;
	int errs = 0;
	for (i = 0; i<elems - 1; i++)
	if (numbers[i]>numbers[i + 1])
		errs++;
	return(errs);
}

void qs(int *lista, int limite_izq, int limite_der)
{
	int izq, der, temporal, pivote;

	izq = limite_izq;
	der = limite_der;
	pivote = lista[(izq + der) / 2];

	do{
		while (lista[izq] < pivote && izq < limite_der)izq++;
		while (pivote<lista[der] && der > limite_izq)der--;
		if (izq <= der)
		{
			temporal = lista[izq];
			lista[izq] = lista[der];
			lista[der] = temporal;
			izq++;
			der--;

		}
	} while (izq <= der);

	if (limite_izq<der){
		#pragma omp task
		qs(lista, limite_izq, der);
	}
	if (limite_der>izq){	
		#pragma omp task
		qs(lista, izq, limite_der);
	}

	#pragma omp taskwait
}

void SortArr(int *numbers)
{
	#pragma omp parallel
		qs(numbers, 0, ELEMS - 1);
}

