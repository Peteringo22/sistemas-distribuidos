#include <stdio.h>
#include <mpi.h>

int main(int argc, char* argv[]){

  	//variables for the range and range size
	int x, y;
	int length;
	
	//get the min and max val
	//fflush(stdin);
	scanf(" %d", &x);

	//fflush(stdin);
	scanf(" %d", &y);

	//calculate range size
	length = y - x + 1;

	int rank, size;

	MPI_Init (&argc, &argv); 

	double mytime;   /*declare a variable to hold the time returned*/
	mytime = MPI_Wtime();  /*get the time just before work to be timed*/

  	MPI_Comm_rank (MPI_COMM_WORLD, &rank);        /* get current process id */
  	MPI_Comm_size (MPI_COMM_WORLD, &size);        /* get number of processes */

  	//printf( "Hello world from process %d of %d\n", rank, size );
  	
	int i = 0, k = 0, j = 0;
	int data[3] = {x, y, length};

	//if is the master process
	MPI_Bcast(data, 3, MPI_INT, 0, MPI_COMM_WORLD);

	//printf("x = %d - y = %d - length = %d\n", data[0], data[1], data[2]);

	MPI_Barrier(MPI_COMM_WORLD);

	x = data[0];
	y = data[1];
	length = data[2];

	//array for the main proccess
	int array[length];

	if(rank == 0){
		for(i = 0; i < length; i++){
			array[i]=0;
		}
	}

	MPI_Barrier(MPI_COMM_WORLD);

	int index=rank; 
	int counter = 0;
	int pro_zero_counter = 0;

	for(i = x + rank; i <= y; i+=size){
	    for(k=2;k<i;k++){
	   		long z=i%k;
	        if(z==0)
	       		counter += k;
	    }
	    counter += 1;
	    if(rank != 0){//manda el mensaje
	    	int sum_array[2] = { i, counter };
	    	MPI_Send(sum_array, 2, MPI_INT, 0, 1, MPI_COMM_WORLD);
	    }
	    else{
	    	array[index] = counter;
	    	pro_zero_counter++;
		    /*int rec_array[2];
		   	MPI_Recv(rec_array, 2, MPI_INT, MPI_ANY_SOURCE, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		   	printf("index %d sum %d", rec_array[0], rec_array[1]);*/
	    }

	    index += size;
	    counter = 0;
	}

	MPI_Barrier(MPI_COMM_WORLD);

	if(rank == 0){
		for(i = 0; i < length - pro_zero_counter; i++){
			//receive the message of the sum data<
			int rec_array[2];
			MPI_Recv(rec_array, 2, MPI_INT, MPI_ANY_SOURCE, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
			//rec_arra[0] is the index, rec_array[1] is the sum
			array[rec_array[0] - x] = rec_array[1];
		}

		for(i = 0; i < length; i++){
			int number = array[i];
			if(array[i] != 1 & number - x < length && array[i] != i + x){
				if(i + x == array[number - x])
					printf("%d - %d son amigos\n", i + x, number);
			}
		}

		mytime = MPI_Wtime() - mytime; /*get the time just after work is done
                                    and take the difference */
   		printf("Time %lf seconds.\n",mytime);		
	}
	
	MPI_Finalize();
}
