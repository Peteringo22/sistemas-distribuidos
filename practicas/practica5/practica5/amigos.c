#include <stdio.h>
#include <sys/time.h>

int main(){

	struct timeval tvalBefore, tvalAfter;  // removed comma

	int x, y;
	int length;
	scanf("%d", &x);
	scanf("%d", &y);
	gettimeofday (&tvalBefore, NULL);
	printf("You entered x %d - y %d \n", x, y);
	length = y - x;

	int array[length];

	int index=0, i = 0, k = 0, j = 0;

	for(i = 0; i < length; i++)
		array[i]=0;

	for(i = x; i <= y; i++){
	    for(k=2;k<i;k++){
	   		long z=i%k;
	        if(z==0)
	       		array[index] += k;
	    }
	    array[index] += 1;
	    index++;
	}

	for(i = 0; i < length; i++){
		for(j = 0; j < length; j++ ){
			if(i != j)
				if(array[i] != 1 & array[i] == array[j])
					printf("%d - %d son amigos\n", i + x, j + x);
		}
	}

	gettimeofday (&tvalAfter, NULL);

    // Changed format to long int (%ld), changed time calculation

    printf("Time in microseconds: %f microseconds\n", (((tvalAfter.tv_sec - tvalBefore.tv_sec)*1000000L+tvalAfter.tv_usec) - tvalBefore.tv_usec ) / 1000000.0 ); // Added semicolon

}
