﻿#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <windows.h>
#define DIF 16
#define NHILOS 4
#define STACK_SIZE (4000 * 3000)

// NOMBRE DEL ARCHIVO A PROCESAR
char filename[] = "Planets.bmp";
#pragma pack(2)  // Empaquetado de 2 bytes

int imageRows, imageCols;//image global rows and cols
char namedest[80];//name dest

typedef struct {
	unsigned char magic1;
	unsigned char magic2;
	unsigned int size;
	unsigned short int reserved1, reserved2; unsigned int pixelOffset; // offset a la imagen
} HEADER;

#pragma pack() // Empaquetamiento por default typedef struct {

typedef struct {
	unsigned int size; // Tamaño de este encabezado INFOHEADER 
	int cols, rows; // Renglones y columnas de la imagen unsigned 
	short int planes;
	unsigned short int bitsPerPixel; // Bits por pixel
	unsigned int compression;
	unsigned int cmpSize;
	int xScale, yScale;
	unsigned int numColors;
	unsigned int importantColors;
} INFOHEADER;

typedef struct {
	unsigned char red;
	unsigned char green;
	unsigned char blue;
} PIXEL;

typedef struct {
	HEADER header;
	INFOHEADER infoheader;
	PIXEL *pixel;
} IMAGE;

typedef struct {
	IMAGE *imagefte;
	IMAGE *imagedst;
	int id;
} bmpStruct;

IMAGE imagenfte, imagendst;


int loadBMP(char *filename, IMAGE *image) {
	FILE *fin;
	int i = 0;
	int totpixs = 0;
	fin = fopen(filename, "rb+");

	// Si el archivo no existe
	if (fin == NULL)
		return(-1);

	// Leer encabezado
	fread(&image->header, sizeof(HEADER), 1, fin);

	// Probar si es un archivo BMP
	if (!((image->header.magic1 == 'B') && (image->header.magic2 == 'M'))) return(-1);

	fread(&image->infoheader, sizeof(INFOHEADER), 1, fin);
	//printf("Bits per Pixel: %u --- Compression: %d\n", image->infoheader.bitsPerPixel, image->infoheader.compression);

	// Probar si es un archivo BMP 24 bits no compactado
	if (!((image->infoheader.bitsPerPixel == 24) && !image->infoheader.compression))
		return(-1);


	image->pixel = (PIXEL *)malloc(sizeof(PIXEL)*image->infoheader.cols*image->infoheader.rows); totpixs = image->infoheader.rows*image->infoheader.cols;
	while (i<totpixs)
	{
		fread(image->pixel + i, sizeof(PIXEL), 512, fin); i += 512;
	}
	fclose(fin);

	return 0;
}

int saveBMP(char *filename, IMAGE *image) {
	FILE *fout;
	int i, totpixs;
	fout = fopen(filename, "wb");
	if (fout == NULL)
		return(-1); // Error // Escribe encabezado

	fwrite(&image->header, sizeof(HEADER), 1, fout); // Escribe información del encabezado
	fwrite(&image->infoheader, sizeof(INFOHEADER), 1, fout);
	i = 0;
	totpixs = image->infoheader.rows*image->infoheader.cols;
	while (i<totpixs)
	{
		fwrite(image->pixel + i, sizeof(PIXEL), 512, fout); i += 512;
	}

	fclose(fout);

	return 0;
}

unsigned char blackandwhite(PIXEL p)
{
	return((unsigned char)(0.3*((float)p.red) + 0.59*((float)p.green) + 0.11*((float)p.blue)));
}

//Thread function
DWORD WINAPI threadfunc(LPVOID arg)
{
	bmpStruct* thread = (bmpStruct *)arg;//we get the struct that contains the images and the process id
	int i = 0, j = 0;
	//printf("thread id: %d", thread->id);
	PIXEL *v0, *v1, *v2, *v3, *v4, *v5, *v6, *v7;
	PIXEL *pfte, *pdst;

	//start i at process id, and increment in the number of processes it execute so each process execute a segmente of the image matrix
	for (i = thread->id; i<imageRows - 1; i += NHILOS)
	{
		pfte = thread->imagefte->pixel + imageCols*i + j;

		v0 = pfte - imageCols - 1;
		v3 = pfte - 1;
		v5 = pfte + imageCols - 1;
		v1 = pfte - imageCols;
		v6 = pfte + imageCols;
		v2 = pfte - imageCols + 1;
		v4 = pfte + 1;
		v7 = pfte + imageCols + 1;

		//store a 3x3 matrix at the start of each row
		int tempArray[] = {
			abs(blackandwhite(*pfte) - blackandwhite(*v0)>DIF)
			, abs(blackandwhite(*pfte) - blackandwhite(*v3)>DIF)
			, abs(blackandwhite(*pfte) - blackandwhite(*v5)>DIF)
			, abs(blackandwhite(*pfte) - blackandwhite(*v1)>DIF)
			, abs(blackandwhite(*v4) - blackandwhite(*pfte)>DIF)//X
			, abs(blackandwhite(*pfte) - blackandwhite(*v6)>DIF)
			, abs(blackandwhite(*pfte) - blackandwhite(*v2)>DIF)
			, abs(blackandwhite(*pfte) - blackandwhite(*v4)>DIF)
			, abs(blackandwhite(*pfte) - blackandwhite(*v7)>DIF)
		};

		for (j = 1; j<imageCols - 1; j++)
		{

			if (j != 1){

				pfte = thread->imagefte->pixel + imageCols*i + j;

				//as we go through the matrix we only need to to compute the next 3 values of the cols in each row
				v2 = pfte - imageCols + 1;
				v4 = pfte + 1;
				v7 = pfte + imageCols + 1;

				//put the 2nd and 3rd column of our matrix in the 1st and 2nd column, as if we push it to the left
				tempArray[0] = tempArray[3];
				tempArray[1] = tempArray[4];
				tempArray[2] = tempArray[5];

				tempArray[3] = tempArray[6];
				tempArray[4] = tempArray[7];
				tempArray[5] = tempArray[8];

				//the 3rd column is assign to the next column right to the pixel pfte
				tempArray[6] = abs(blackandwhite(*pfte) - blackandwhite(*v2))>DIF;
				tempArray[7] = abs(blackandwhite(*pfte) - blackandwhite(*v4))>DIF;
				tempArray[8] = abs(blackandwhite(*pfte) - blackandwhite(*v7))>DIF;
			}

			pdst = thread->imagedst->pixel + imageCols*i + j;

			if (
				tempArray[1] || tempArray[6] || tempArray[7] || tempArray[8] ||
				tempArray[0] || tempArray[2] || tempArray[3] || tempArray[5]
				)
			{
				pdst->red = 0;
				pdst->green = 0;
				pdst->blue = 0;
			}
			else
			{
				pdst->red = 255;
				pdst->green = 255;
				pdst->blue = 255;
			}
		}

	}

	return 0;
}


void processBMP(IMAGE *imagefte, IMAGE *imagedst) {

	HANDLE threads[NHILOS];
	bmpStruct args[NHILOS];

	memcpy(imagedst, imagefte, sizeof(IMAGE)-sizeof(PIXEL *));
	imageRows = imagefte->infoheader.rows;
	imageCols = imagefte->infoheader.cols;
	int i;
	imagedst->pixel = (PIXEL *)malloc(sizeof(PIXEL)*imageRows*imageCols);

	for (i = 1; i <= NHILOS; i++)
	{
		args[i - 1] = { imagefte, imagedst, i };
		threads[i - 1] = CreateThread(NULL, 0, threadfunc, &args[i - 1], 0, NULL);
	}

	//wait to all threads to finish
	WaitForMultipleObjects(NHILOS, threads, TRUE, INFINITE);
}

int main()
{
	int res;
	clock_t ts, te;

	ts = clock();

	strcpy(namedest, strtok(filename, "."));

	strcat(filename, ".bmp");
	strcat(namedest, "_P.bmp");
	printf("Archivo fuente %s\n", filename);
	printf("Archivo destino %s\n", namedest);
	res = loadBMP(filename, &imagenfte);
	if (res == -1)
	{
		fprintf(stderr, "Error al abrir imagen\n");
		exit(1);
	}

	printf("Procesando imagen de: Renglones = %d, Columnas = %d\n", imagenfte.infoheader.rows, imagenfte.infoheader.cols);
	processBMP(&imagenfte, &imagendst);

	res = saveBMP(namedest, &imagendst);
	if (res == -1)
	{
		fprintf(stderr, "Error al escribir imagen\n");
		exit(1);
	}

	te = clock();
	printf("Tiempo %3.6f segundos\n", ((float)te - (float)ts) / CLOCKS_PER_SEC);

}